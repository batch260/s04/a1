<?php


class Building {

	private $name;
	private $floors;
	private $address;


	public function __construct($name, $floors, $address) {
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}


	//Name
	public function getName() {
		return $this->name;
	}

	 public function setName($name) {
	  $this->name = $name;
	 }


	 //floors
	public function getFloors() {
		return $this->floors;
	}

	 public function setFloors($floors) {
	  $this->floors = $floors;
	 }

	 //address

	public function getAddress() {
		return $this->address;
	}

	 public function setAddress($address) {
	  $this->address = $address;
	 }


}


class Condominium extends Building {

}


$building = new Building('Caswyn', 8, 'Timog Avenue, Quezon City, Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');